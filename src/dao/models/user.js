const { Schema, model } = require("mongoose");
const findOrCreate = require("mongoose-findorcreate");

const userSchema = new Schema({
  username: {
    unique: true,
    type: String,
  },
  hash: String,
  facebookId: String,
}).plugin(findOrCreate);

module.exports = model("User", userSchema);
