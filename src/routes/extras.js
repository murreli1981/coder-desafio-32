const { info, forceShutdown, randoms } = require("../controllers/extras");

module.exports = (router) => {
  router.get("/info", info);
  router.post("/forceShutdown", forceShutdown);
  router.get("/random", randoms);
  return router;
};
